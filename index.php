<?php

$klay = './klay';
$site = './site';

// system path globals
define('ROOT', str_replace('\\', '/', __DIR__ . '/'));
define('KLAY', str_replace(ROOT, '', str_replace('\\', '/', realpath($klay) . '/')));
define('SITE', str_replace(ROOT, '', str_replace('\\', '/', realpath($site) . '/')));

// remove global variables
unset($klay, $site);

// call klay initializer
require KLAY . 'klay.php';