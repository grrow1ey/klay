<?php

namespace Addons\Captcha;

class Macros extends \Macro
{
	public static function index($node, $writer)
	{
		return $writer->write('echo \'<img src="/TRIGGERS/captcha/index?\' . \Helpers::queryString(\'%node.args\') . \'"/>\'');
	}
}