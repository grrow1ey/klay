<?php

namespace Addons\Captcha;

class Triggers extends \Trigger
{
	public function index($app, $params)
	{
		$font    = '/fonts/captcha.ttf';
		$size    = intval(($app->get('GET.size') ?: 32) / 2);
		$length  = intval($app->get('GET.length') ?: 5);
		$fground = $app->get('GET.foreground') ?: 'FFFFFF';
		$bground = $app->get('GET.background') ?: '000000';
		$captcha = new \Image();
		$captcha->captcha($font, $size, $length, 'SESSION._captcha', __DIR__, $this->color($fground), $this->color($bground));
		echo $captcha->render();
	}
	
	private function color($string)
	{
		$string = str_replace(['0x', '#'], '', $string);
		return hexdec($string);
	}
}