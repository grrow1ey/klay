<?php

namespace Addons\Content;

class Api extends \Api
{
    protected $app;

    protected $query;

    public function __construct()
    {
        $this->app   = \Base::instance();

        $this->query = new \DB\Cortex(new \DB\Jig($this->app->get('DATA')), 'content.json');

		$ext = $this->app->get('KLAY.ext.content');

        foreach (\Helpers::recursiveGlob($this->app->cpath(SITE . $this->app->get('SITE.content') . '*' . $ext)) ?: [] as $file) {
            $this->query->load(['@_uid=?', $this->app->hash($file)]);
            if ($this->query->dry()) {
                $content = [
                    '_uid'     => $this->app->hash($file),
                    '_path'    => $file,
					'_slug'    => basename($file, $ext),
					'_route'   => str_replace([$this->app->cpath(SITE . $this->app->get('SITE.content')), $ext], '', $file),
                    '_updated' => filemtime($file),
                ];
				$content += \Helpers::parseFrontMatter($this->app->read($file));
                $this->query->copyfrom($content);
                $this->query->save();
            } elseif ($this->query->_updated < filemtime($file)) {
                $content = \Helpers::parseFrontMatter($this->app->read($file));
                $this->query->copyfrom($content);
                $this->query->set('_updated', filemtime($file));
                $this->query->save();
            }
            $this->query->reset();
		}
    }

    public function get($filters, $options = null)
    {
        $this->query->reset();
        $this->query->load(\Helpers::cleanFilters($filters), $options);
        if ($this->query->valid()) {
            return $this->query->cast();
        }
        return false;
    }

    public function find($filters = null, $options = null)
    {
        $results = $this->query->find(\Helpers::cleanFilters($filters), $options);
        if ($results) {
            return $results->castAll();
        }
        return false;
	}

	public function engine()
	{
		return $this->query;
	}
	
	public function __debugInfo() {}
}
