<?php

namespace Addons\Mailer;

class Api extends \Api
{
    protected $smtp;
    protected $recipients;
    protected $message;
    protected $charset;

    /**
     * create mailer instance working with a specific charset
     * usually one of these:
     *      ISO-8859-1
     *      ISO-8859-15
     *      UTF-8
     * @param string $enforceCharset
     */
    public function __construct($enforceCharset = 'ISO-8859-15')
    {
        $this->charset = $enforceCharset;
        $this->recipients = [];
        $this->initSMTP();
    }

    /**
     * initialize SMTP plugin
     */
    public function initSMTP()
    {
        $app = \Base::instance();
        $this->smtp = new \SMTP(
            $app->get('SETTINGS._mailer._host'),
            $app->get('SETTINGS._mailer._port'),
            $app->get('SETTINGS._mailer._scheme'),
            $app->get('SETTINGS._mailer._username'),
            $app->get('SETTINGS._mailer._password')
        );
        if ($app->exists('SETTINGS._mailer.errors_to', $errors_to) && !empty($errors_to)) {
            $this->smtp->set('Errors-to', '<'.$errors_to.'>');
        }
        if ($app->exists('SETTINGS._mailer.return_to', $return_to) && !empty($return_to)) {
            $this->smtp->set('Return-Path', '<'.$return_to.'>');
        }
        if ($app->exists('SETTINGS._mailer.from_mail', $from_mail) && !empty($from_mail)) {
            $from_name = !$app->devoid('SETTINGS._mailer.from_name') ? $app->get('SETTINGS._mailer.from_name') : null;
            $this->setFrom($from_mail, $from_name);
		}
    }

    /**
     * encode special chars if possible
     * @param $str
     * @return mixed
     */
    protected function encode($str)
    {
        if (empty($str) || $this->charset == 'UTF-8') {
            return $str;
        }
        if (extension_loaded('iconv')) {
            $out = @iconv("UTF-8", $this->charset . "//IGNORE", $str);
        }
        if (!isset($out) || !$out) {
            $out = extension_loaded('mbstring')
                ? mb_convert_encoding($str, $this->charset, "UTF-8")
                : utf8_decode($str);
        }
        return $out ?: $str;
    }

    /**
     * build email with title string
     * @param $email
     * @param null $title
     * @return string
     */
    protected function buildMail($email, $title = null)
    {
		$mail  = $title ? '"' . $title . '" ' : '';
		$mail .= '<' . $email . '>';
        return $mail;
    }

    /**
     * set encoded header value
     * @param $key
     * @param $val
     */
    public function set($key, $val)
    {
        $this->smtp->set($key, $this->encode($val));
    }

    /**
     * set message sender
     * @param $email
     * @param null $title
     */
    public function setFrom($email, $title = null)
    {
        $this->set('From', $this->buildMail($email, $title));
    }

    /**
     * add a direct recipient
     * @param $email
     * @param null $title
     */
    public function addTo($email, $title = null)
    {
        $this->recipients['To'][$email] = $title;
    }

    /**
     * add a carbon copy recipient
     * @param $email
     * @param null $title
     */
    public function addCc($email, $title = null)
    {
        $this->recipients['Cc'][$email] = $title;
    }

    /**
     * add a blind carbon copy recipient
     * @param $email
     * @param null $title
     */
    public function addBcc($email, $title = null)
    {
        $this->recipients['Bcc'][$email] = $title;
    }

    /**
     * reset recipients if key was given, or restart whole smtp plugin
     * @param null $key
     */
    public function reset($key = null)
    {
        if ($key) {
            $key = ucfirst($key);
            $this->smtp->clear($key);
            if (isset($this->recipients[$key])) {
                unset($this->recipients[$key]);
            }
        } else {
            $this->recipients = [];
            $this->initSMTP();
        }
    }

    /**
     * set message in plain text format
     * @param $message
     */
    public function setText($message)
    {
        $this->message['text'] = $message;
    }

    /**
     * set message in HTML text format
     * @param $message
     */
    public function setHTML($message)
    {
        $app = \Base::instance();
        // we need a clean template instance for extending it one-time
        $tmpl = new \Template();
        // create traceable jump links
        if ($app->exists('SETTINGS._mailer.jumplinks', $jumplink) && $jumplink) {
            $tmpl->extend('a', function ($node) use ($app, $tmpl) {
                if (isset($node['@attrib'])) {
                    $attr = $node['@attrib'];
                    unset($node['@attrib']);
                } else {
                    $attr = [];
                }
                if (isset($attr['href'])) {
                    if (!$app->exists('SETTINGS._mailer.jump_route', $ping_route)) {
                        $ping_route = '/mailer-jump';
                    }
                    $attr['href'] = $app->get('SCHEME').'://'.$app->get('HOST').$app->get('BASE').
                        $ping_route . '?target=' . urlencode($attr['href']);
                }
                $params = '';
                foreach ($attr as $key => $value) {
                    $params .= ' ' . $key . '="' . $value . '"';
                }
                return '<a' . $params . '>' . $tmpl->build($node) . '</a>';
            });
        }
        $message = $tmpl->build($tmpl->parse($message));
        $this->message['html'] = $message;
    }

    /**
     * add a file attachment
     * @param $path
     * @param null $alias
     * @param null $cid
     */
    public function attachFile($path, $alias = null, $cid = null)
    {
        $this->smtp->attach($path, $alias, $cid);
    }

    /**
     * send message
     * @param $subject
     * @param bool $mock
     * @return bool
     */
    public function send($subject, $mock = false)
    {
        foreach ($this->recipients as $key => $rcpts) {
            $mails = [];
            foreach ($rcpts as $mail=>$title) {
                $mails[] = $this->buildMail($mail, $title);
			}
            $this->set($key, implode(', ', $mails));
		}
		$this->smtp->set('Subject', $this->encode($subject));
        $body = '';
        $hash = uniqid(null, true);
        $eol  = "\r\n";
        if (isset($this->message['text']) && isset($this->message['html'])) {
            $this->smtp->set('Content-Type', 'multipart/alternative; boundary="'.$hash.'"');
            $body .= '--' . $hash . $eol;
            $body .= 'Content-Type: text/plain; charset=' . $this->charset . $eol;
            $body .= $this->message['text'] . $eol . $eol;
            $body .= '--' . $hash . $eol;
            $body .= 'Content-Type: text/html; charset=' . $this->charset . $eol . $eol;
            $body .= $this->message['html'] . $eol;
        } elseif (isset($this->message['text'])) {
            $this->smtp->set('Content-Type', 'text/plain; charset=' . $this->charset);
            $body = $this->message['text'] . $eol;
        } elseif (isset($this->message['html'])) {
            $this->smtp->set('Content-Type', 'text/html; charset=' . $this->charset);
            $body = $this->message['html'] . $eol;
		}
		$success = $this->smtp->send($this->encode($body), 'verbose', $mock);
        $app = \Base::instance();
        if (!$success && $app->exists('SETTINGS._mailer.on.failure', $fail_handler)) {
            $app->call($fail_handler, [$this,$this->smtp->log()]);
		}
		return $success;
    }

    /**
     * save the send mail to disk
     * @param $filename
     */
    public function save($filename)
    {
		$out   = '';
        $app   = \Base::instance();
        $lines = explode("\n", $this->smtp->log());
        $start = false;
        for ($i = 0, $max = count($lines); $i < $max; $i++) {
            if (!$start && preg_match('/^354.*?$/', $lines[$i], $matches)) {
                $start = true;
                continue;
            } elseif (preg_match(
                '/^250.*?$\s^QUIT/m',
                $lines[$i] . ($i+1 < $max ? "\n" . $lines[$i+1] : ''),
                $matches
            )) {
                break;
            }
            if ($start) {
                $out .= $lines[$i] . "\n";
            }
        }
        if ($out) {
            $path = $app->get('SETTINGS._mailer.storage_path');
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $app->write($path . $filename, $out);
        }
    }

    /**
     * expose smtp log
     * @return mixed
     */
    public function log()
    {
        return $this->smtp->log();
    }

    /**
     * receive and proceed message ping
     * @param Base $app
     * @param $params
     */
    public function ping(\Base $app, $params)
    {
        $hash = $params['hash'];
        // trigger ping event
        if ($app->exists('SETTINGS._mailer.on.ping', $ping_handler)) {
            $app->call($ping_handler, array($hash));
        }
        $img = new \Image();
        // 1x1 transparent 8bit PNG
        $img->load(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMA'.
            'AAAl21bKAAAABGdBTUEAALGPC/xhBQAAAANQTFRFAAAAp3o92gAAAAF0U'.
            'k5TAEDm2GYAAAAKSURBVAjXY2AAAAACAAHiIbwzAAAAAElFTkSuQmCC'));
        $img->render();
    }

    /**
     * track clicked link and reroute
     * @param Base $app
     */
    public function jump(\Base $app, $params)
    {
        $target = $app->get('GET.target');
        // trigger jump event
        if ($app->exists('SETTINGS._mailer.on.jump', $jump_handler)) {
            $app->call($jump_handler, array($target,$params));
        }
        $app->reroute(urldecode($target));
    }

    /**
     * init routing
     */
    public static function initTracking()
    {
        /** @var \Base $app */
        $app = \Base::instance();
        if (!$app->exists('SETTINGS._mailer.ping_route', $ping_route)) {
            $ping_route = '/mailer-ping/@hash.png';
        }
        $app->route('GET ' . $ping_route, '\Addons\Contact\Api->ping');

        if (!$app->exists('SETTINGS._mailer.jump_route', $jump_route)) {
            $jump_route = '/mailer-jump';
        }
        $app->route('GET ' . $jump_route, '\Addons\Contact\Api->jump');
    }
}
