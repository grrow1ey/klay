<?php

namespace Addons\Mailer;

class Macros extends \Macro
{
	public static function contact($node, $writer)
    {
        return $writer->write('echo \'<form action="/TRIGGERS/mailer/contact?\' . \Helpers::queryString(\'%node.args\') . \'" method="POST">\'');
    }

    public static function contactEnd($node, $writer)
    {
        return $writer->write('echo \'</form>\'');
    }
}