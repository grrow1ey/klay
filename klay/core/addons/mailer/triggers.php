<?php

namespace Addons\Mailer;

class Triggers extends \Trigger
{
	public function contact($app, $params)
	{
		$log    = new \Log('addons_mailer_triggers.log');
		$post   = $app->get('POST');
		$flash  = \Flash::instance();
		$rules  =  [
			'_to'      => 'required|valid_email',
			'_from'    => 'required|valid_email',
            '_subject' => 'required|max_len,100',
            '_message' => 'required|max_len,255'
		];
		if (array_key_exists('_captcha', $post) && $app->get('SESSION._captcha')) {
			$rules['_captcha'] = 'required|captcha';
		}
		if ($app->exists('GET.validation', $validation)) {
			$fields = explode(',', $validation);
			foreach ($fields as $rule) {
				foreach (explode(':', $rule) ?: [] as $f => $v) {
					if (!array_key_exists($f, $rules)) {
						$rules[$f] = $v;
					}
				}
			}
		}
		$flash->set('mailer_contact_post', $post);
		$valid  = \Validate::isValid($post, $rules);
		$return = $_SERVER['HTTP_REFERER'] ?: '/';
		if ($valid === true) {
			try {
				$contact = new Api();
				$contact->setFrom($post['_from']);
				$contact->addTo($post['_to']);
				$contact->setText($post['_message']);
				if (!$app->get('GET.template') && $contact->send(\Spyc::YAMLDump($post, true, true, true))) {
					$flash->add($app->get('LANG.ADDONS.MAILER.contact_success'), 'success');
				} else {
					$config = \Klay::defaults() + \Klay::config() + $post;
					$output = Theme::instance($config)->render($app->get('GET.template', $config));
				}
				if ($app->exists('GET.onsuccess', $url)) {
					$return = $url;
				}
				$log->write($contact->log());
			} catch (\Exception $e) {
				$flash->add($app->get('LANG.ADDONS.MAILER.contact_failure'), 'error');
				if ($app->exists('GET.onerror', $url)) {
					$return = $url;
				}
			}
			$log->write($contact->log());
        } else {
            $flash->add($app->get('LANG.ADDONS.MAILER.contact_failure'), 'error');
            if (count($valid)) {
                $flash->set('mailer_contact_errors', $valid);
            }
            if ($app->exists('GET.onerror', $url)) {
                $return = $url;
            }
		}
		header('Location: ' . $return);
	}
}