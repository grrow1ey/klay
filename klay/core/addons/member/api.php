<?php

namespace Addons\Member;

class Api extends \Api
{
    protected $app;

    protected $query;

    public function __construct()
    {
        $this->app   = \Base::instance();

        $this->query = new \DB\Cortex(new \DB\Jig($this->app->get('DATA')), 'members.json');

        foreach (\Helpers::recursiveGlob($this->app->cpath(SITE . $this->app->get('SITE.members') . '*.md')) ?: [] as $file) {
            $this->query->load(['@_username=?', basename($file, '.md')]);
            if ($this->query->dry()) {
                $member = [
					'_uid'      => $this->app->hash($file),
					'_path'     => $file,
                    '_avatar'   => $this->makeAvatar($this->app->hash($file)),
                    '_username' => basename($file, '.md'),
                    '_updated'  => filemtime($file),
                ];
                $member += \Helpers::parseFrontMatter($this->app->read($file), '---', true, '_bio');
                if (isset($member['_password'])) {
                    $member['_password_hash'] = password_hash($member['_password'], PASSWORD_BCRYPT);
                    unset($member['_password']);
                    $this->updateUser($member['_username'], $member);
                }
                $this->query->copyfrom($member);
                $this->query->save();
            } elseif ($this->query->_updated < filemtime($file)) {
                $member = \Helpers::parseFrontMatter($this->app->read($file), '---', true, '_bio');
                if (isset($member['_password'])) {
                    $member['_password_hash'] = password_hash($member['_password'], PASSWORD_BCRYPT);
                    unset($member['_password']);
                    $this->updateUser($member['_username'], $member);
                }
                $this->query->copyfrom($member);
                $this->query->set('_updated', filemtime($file));
                $this->query->save();
            }
            $this->query->reset();
        }
    }

    public function get($filters, $options = null)
    {
        $this->query->reset();
        $this->query->load(\Helpers::cleanFilters($filters), $options);
        if ($this->query->valid()) {
            return $this->query->cast();
        }
        return false;
    }

    public function find($filters = null, $options = null)
    {
        $results = $this->query->find(\Helpers::cleanFilters($filters), $options);
        if ($results) {
            return $results->castAll();
        }
        return false;
    }

    public function login($username, $password, $cookie = true)
    {
        $this->query->load(['@_username=?', $username]);
        if ($this->query->valid() && password_verify($password, $this->query->_password_hash)) {
            $member = $this->query->cast();
            $member['_token'] = $this->encodeToken([
                '_avatar'   => $member['_avatar'],
                '_username' => $member['_username'],
                '_rights'   => $member['_rights'],
            ]);
            unset($member['_password_hash']);
            if ($cookie) {
                $this->app->set('COOKIE.klayMemberToken', $member['_token']);
            }
            return $member;
        }
        return false;
    }

    public function logout()
    {
		$this->app->clear('SESSION');
        $this->app->clear('COOKIE.klayMemberToken');
    }

    public function current()
    {
        if ($this->app->exists('COOKIE.klayMemberToken', $token)) {
            try {
                $member = $this->decodeToken($token);
                if ($member && $member->data) {
                    return get_object_vars($member->data);
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
	}
	
	public function engine()
	{
		return $this->query;
	}

    private function makeAvatar($uid)
    {
        $avi = $this->app->cpath(SITE . $this->app->get('SITE.avatars') . $uid . '.png');
        if (!$this->app->read($avi)) {
            $img = new \Image();
            $img->identicon($uid, 200, rand(2, 7));
            $this->app->write($avi, $img->dump('png', 9));
        }
        return '/' . $avi;
    }

    private function decodeToken($token)
    {
        return \Firebase\JWT\JWT::decode($token, sha1($this->app->get('SEED')), ['HS256']);
    }

    private function encodeToken($data = [], $expires = null)
    {
        $token  = [
            'iss' => $this->app->get('HOST'),
            'aud' => $this->app->get('IP'),
            'iat' => time(),
            'nbf' => time() - 1,
        ];
        $token['data'] = $data;
        return \Firebase\JWT\JWT::encode($token, sha1($this->app->get('SEED')));
    }

    private function updateUser($member, $data)
    {
        $member = $this->app->cpath(SITE . $this->app->get('SITE.members') . $member . '.md');
        if (file_exists($member)) {
            unset($data['_uid'], $data['_avatar'], $data['_username'], $data['_updated']);
            $this->app->write($member, \Helpers::createFrontMatter($data, '---', true, '_bio'));
        }
	}
	
	public function __debugInfo() {}
}
