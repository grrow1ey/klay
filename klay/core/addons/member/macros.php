<?php

namespace Addons\Member;

class Macros extends \Macro
{
    public static function login($node, $writer)
    {
        return $writer->write('echo \'<form action="/TRIGGERS/member/login?\' . \Helpers::queryString(\'%node.args\') . \'" method="POST">\'');
    }

    public static function loginEnd($node, $writer)
    {
        return $writer->write('echo \'</form>\'');
    }

    public static function logout($node, $writer)
    {
        return $writer->write('echo \'<a href="/TRIGGERS/member/logout?\' . \Helpers::queryString(\'%node.args\') . \'">\'');
    }

    public static function logoutEnd($node, $writer)
    {
        return $writer->write('echo \'</a>\'');
    }
}
