<?php

namespace Addons\Member;

class Triggers extends \Trigger
{
    public function login($app, $params)
    {
		$post   = $app->get('POST');
		$flash  = \Flash::instance();
		$rules  =  [
            '_username' => 'required',
            '_password' => 'required'
		];
		if (array_key_exists('_captcha', $post) && $app->get('SESSION._captcha')) {
			$rules['_captcha'] = 'required|captcha';	
		}
		if ($app->exists('GET.validation', $validation)) {
			$fields = explode(',', $validation);
			foreach ($fields as $rule) {
				foreach (explode(':', $rule) ?: [] as $f => $v) {
					if (!array_key_exists($f, $rules)) {
						$rules[$f] = $v;
					}
				}
			}
		}
		$flash->set('member_login_post', $post);
        $valid  = \Validate::isValid($post, $rules);
		$return = $_SERVER['HTTP_REFERER'] ?: '/';
        if ($valid === true && $member = Api::instance()->login($post['_username'], $post['_password'])) {
            $flash->add($app->get('LANG.ADDONS.MEMBER.login_success'), 'success');
            if ($app->exists('GET.onsuccess', $url)) {
                $return = $url;
            }
        } else {
            $flash->add($app->get('LANG.ADDONS.MEMBER.login_failure'), 'error');
            if (count($valid)) {
                $flash->set('member_login_errors', $valid);
            }
            if ($app->exists('GET.onerror', $url)) {
                $return = $url;
            }
		}
        header('Location: ' . $return);
    }

    public function logout($app, $params)
    {
        $return = $_SERVER['HTTP_REFERER'] ?: '/';
        if ($app->exists('GET.onsuccess', $url)) {
            $return = $url;
        }
        Api::instance()->logout();
        \Flash::instance()->add($app->get('LANG.ADDONS.MEMBER.logout_success'), 'success');
        header('Location: ' . $return);
    }
}
