<?php

namespace Addons\Pug;

class Filters
{
    public static function index($string)
    {
		$pug = new \Pug([
			'pretty' => true
		]);
        print $pug->displayString($string);
    }
}
