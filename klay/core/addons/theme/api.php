<?php

namespace Addons\Theme;

use Latte\Engine;
use Latte\Macros\MacroSet;
use Tracy\Debugger as Debug;

class Api extends \Prefab
{
	private $data;

	private $engine;

	private $macros;

	public function __construct($data)
	{
		$this->app    = \Base::instance();
		$this->data   = $data;
		$this->engine = new Engine();
		$this->macros = new MacroSet($this->engine->getCompiler());
		if (!$this->data['_debug']) {
			$this->engine->setAutoRefresh(false);
			$this->engine->setTempDirectory($this->app->get('TEMP'));
		}
		if ($addons = \Klay::get('_addons', false)) {
			foreach ($addons ?: [] as $name => $addon) {
				if (isset($addon['_filters'])) {
					foreach ($addon['_filters'] ?: [] as $filter) {
						$filter = strtolower($filter);
						$call   = $addon['_namespace'] . '\\filters::' . $filter;
						$tag    = $filter === 'index' ? $name : $name . '_' . $filter;
						$this->engine->addFilter($tag, $call);
					}
				}
				if (isset($addon['_macros'])) {
					foreach($addon['_macros'] ?: [] as $macro) {
						$macro = strtolower($macro);
						$call  = $addon['_namespace'] . '\\Macros';
						$tag   = $macro === 'index' ? $name : $name . '_' . $macro;
						if (!method_exists($call, $macro . 'end')) {
							$this->macros->addMacro($tag, [$call, $macro]);
						} else {
							$this->macros->addMacro($tag, [$call, $macro], [$call, $macro .'end']);
						}
					}
				}
			}
		}
	}

	public function display($file)
	{
		Debug::barDump($this->data, 'KLAY');
		$this->engine->render($file, $this->data);
	}
}
