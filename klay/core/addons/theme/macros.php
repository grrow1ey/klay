<?php

namespace Addons\Theme;

class Macros extends \Macro
{
    public static function index($node, $writer)
    {
        return $writer->write('echo Base::instance()->cpath($_theme[\'_root\'])');
    }
  
    public static function css($node, $writer)
    {
        return $writer->write('echo Base::instance()->cpath($_theme[\'_root\'], \'css\', %node.word)');
    }

    public static function img($node, $writer)
    {
        return $writer->write('echo Base::instance()->cpath($_theme[\'_root\'], \'img\', %node.word)');
    }

    public static function js($node, $writer)
    {
        return $writer->write('echo Base::instance()->cpath($_theme[\'_root\'], \'js\', %node.word)');
    }
}
