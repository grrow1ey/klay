<?php

class Flash extends Prefab
{
    protected $app;

    protected $msg;

    protected $key;

    public function __construct($key = 'flash')
    {
        $this->app = Base::instance();
        $this->msg = &$this->app->ref('SESSION.' . $key . '.msg');
        $this->key = &$this->app->ref('SESSION.' . $key . '.key');
    }

    public function add($text, $status = 'info')
    {
        $this->msg[] = [
            'status'  => $status,
            'message' => $text
        ];
    }

    public function set($key, $val = true)
    {
        $this->key[$key] = $val;
    }

    public function get($key)
    {
        $out = null;
        if ($this->hasKey($key)) {
            $out = $this->key[$key];
            unset($this->key[$key]);
        }
        return $out;
    }

    public function dump()
    {
        $out = $this->msg;
        $this->clearMessages();
        return $out ?: [];
    }

    private function hasKey($key)
    {
        return ($this->key && array_key_exists($key, $this->key));
    }

    private function hasMessages()
    {
        return !empty($this->msg);
    }

    private function clearMessages()
    {
        $this->msg = [];
	}
	
	public function __debugInfo() {}
}
