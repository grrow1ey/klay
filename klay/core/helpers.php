<?php

class Helpers
{
    public static function arrayGet(&$arr, $path, $separator = '.')
    {
        $keys = explode($separator, $path);
        foreach ($keys as $key) {
            $arr = &$arr[$key];
        }
        return $arr;
    }

    public static function arraySet(&$arr, $path, $value, $separator = '.')
    {
        $keys = explode($separator, $path);
        foreach ($keys as $key) {
            $arr = &$arr[$key];
        }
        $arr = $value;
	}

    public static function cleanFilters($filters)
    {
        foreach ($filters ?: [] as $i => $query) {
            $filters[$i] = preg_replace('/(\w*\=)/', '@$1', $query);
        }
        return $filters;
    }

    public static function createFrontMatter($matter, $sep = '---', $leadSep = true, $bodyKey = 'content')
    {
        $output = $leadSep ? $sep . "\n" : '';
        if (isset($matter[$bodyKey])) {
            $body    = $matter[$bodyKey];
            unset($matter[$bodyKey]);
            $output .= Spyc::YAMLDump($matter, true, false, true);
            $output .= $sep;
            $output .= "\n" . trim($body);
        } else {
            $output .= Spyc::YAMLDump($matter, true, false, true);
            $output .= $sep;
        }
        return $output;
    }

    public static function dirMap($path, $recursive = false)
    {
        $result = [];
        if (is_dir($path) === true) {
            $path  = self::dirPath($path);
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                if (is_dir($path . $file) === true) {
                    $result[$file] = ($recursive === true) ? self::dirMap($path . $file, $recursive) : self::dirSize($path . $file, true);
                } elseif (is_file($path . $file) === true) {
                    $result[$file] = self::dirSize($path . $file);
                }
            }
        } elseif (is_file($path) === true) {
            $result[basename($path)] = self::Size($path);
        }
        return $result;
    }

    public static function dirSize($path, $recursive = true)
    {
        $result = 0;
        if (is_dir($path) === true) {
            $path = self::dirPath($path);
            $files = array_diff(scandir($path), ['.', '..']);
            foreach ($files as $file) {
                if (is_dir($path . $file) === true) {
                    $result += ($recursive === true) ? self::Size($path . $file, $recursive) : 0;
                } elseif (is_file() === true) {
                    $result += sprintf('%u', filesize($path . $file));
                }
            }
        } elseif (is_file($path) === true) {
            $result += sprintf('%u', filesize($path));
        }
        return $result;
    }

    public static function dirPath($path)
    {
        if (file_exists($path) === true) {
            $path = rtrim(str_replace('\\', '/', realpath($path)), '/');
            if (is_dir($path) === true) {
                $path .= '/';
            }
            return $path;
        }
        return false;
    }

    public static function dirIntegrity($path)
    {
        return sha1(serialize(self::dirMap($path, true)));
    }

    public static function emptyDirectory($path)
    {
        $files = glob($path);
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    public static function getRelativePath($from, $to)
    {
        $from    = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
        $to      = is_dir($to) ? rtrim($to, '\/') . '/'   : $to;
        $from    = str_replace('\\', '/', $from);
        $to      = str_replace('\\', '/', $to);
        $from    = explode('/', $from);
        $to      = explode('/', $to);
        $relPath = $to;

        foreach ($from as $depth => $dir) {
            // find first non-matching dir
            if ($dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if ($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }

    public static function isJson($str)
    {
        json_decode($str);
        return (json_last_error() === JSON_ERROR_NONE);
    }

    public static function parseFrontMatter($matter, $sep = '---', $leadSep = true, $bodyKey = '_content')
    {
        $output = [];
        $matter = explode($sep, $matter);
        if ($leadSep) {
            $output = Spyc::YAMLLoadString($matter[1]);
            $output[$bodyKey] = htmlentities(trim($matter[2]));
        } else {
            $output = Spyc::YAMLLoadString($matter[0]);
            $output[$bodyKey] = htmlentities(trim($matter[1]));
        }
        return $output;
    }

    public static function recursiveGlob($pattern, $flags = 0, &$files = [])
    {
        if (is_array($pattern)) {
			foreach($pattern as $p) {
				foreach (glob(dirname($p) . '/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
					$files = array_merge($files, self::recursiveGlob($dir . '/' . basename($p), $flags));
				}
			}
		} else {
			$files = glob($pattern, $flags);
			foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
				$files = array_merge($files, self::recursiveGlob($dir . '/' . basename($pattern), $flags));
			}
		}
        return $files;
    }

    public static function removeCookie($key)
    {
        if (isset($_COOKIE[$key])) {
            unset($_COOKIE[$key]);
            setcookie($key, '', time() - 3600, '/');
        }
    }
  
    public static function stringMatch($pattern, $source)
    {
        $regex = str_replace(['\\*', '\\?'], ['.*', '.'], preg_quote($pattern));
        return preg_match('/^' . $regex . '$/is', $source);
	}
	
	public static function queryString($string)
	{
		$count = null;
		return str_replace(['"','\''], '', preg_replace('/(?:\\"[^"]++\\"|\\\'[^\\\']++\\\')(*SKIP)(*F)|\\s/', '&', $string, -1, $count));
	}
}
