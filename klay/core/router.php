<?php

use Tracy\Debugger as Debug;
use Addons\Theme\Api as Theme;

class Router
{	
	public function __construct($app, $params)
	{
		// find active route
		foreach(Klay::get('_routes') ?: [] as $i => $route) {
			if ($route['_alias'] === $app->get('ALIAS')) {
				Klay::set('_route', $route);
				Klay::set('_params', $params);
				Klay::set('_routes.' . $i . '._active', true);
			}
		}
		// set debugger level
		Debug::enable(Klay::get('_debug') ? Debug::DEVELOPMENT : Debug::PRODUCTION, ROOT . $app->get('LOGS'));
	}

	public function render($app, $params)
	{
		// render route view
		if ($route = Klay::get('_route')) {
			$ext = $app->get('KLAY.ext.view');
			$layout = $route['_layout'] ? $app->cpath(Klay::get('_theme._root'), 'layouts', $route['_layout'] . $ext) : false;
			$template = $route['_template'] ? $app->cpath(Klay::get('_theme._root'), 'templates', $route['_template'] . $ext) : false;
			if ($template) {
				$view = $template;
				Klay::set('_layout', Helpers::getRelativePath($template, $layout));
			} elseif ($layout) {
				$view = $layout;
			}
			// use the theme addon to render the view
			Theme::instance(Klay::defaults() + Klay::get())->display($view);
		}
	}
}
