<?php

abstract class Trigger
{
	protected $request;

	public function __construct($app, $params)
	{
		$this->request = $app->get('GET') + $app->get('POST');
		if ($app->exists('BODY', $body)) {
			if (Helpers::isJson($body)) {
				$this->request += json_decode($body, true);
			} else {
				parse_str($body, $res);
				$this->request += $res;
			}
		}
	}
}