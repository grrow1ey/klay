<?php if (!defined('KLAY') || !defined('SITE')) {
exit('No direct script access allowed!');
}

// include composer autoloader
require KLAY . 'libs/autoload.php';

// quick sytem helper functions
Base::instance()->set('cpath', function () {
	$path  = [];
	foreach (func_get_args() ?: [] as $i => $part) {
		$path[$i] = trim(str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $part), DIRECTORY_SEPARATOR);
	}
	return preg_replace('#/+#', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $path));
});

// klay class for custom configs
final class Klay extends Prefab
{
	private $app;

	public function __construct()
	{
		$this->app = Base::instance();
		if (!$this->app->get('SETTINGS')) {
			// init klay
			$this->initKlay();
			// start listening
			$this->app->run();
		}
	}

	public static function set($key, $default = false)
	{
		Base::instance()->set('SETTINGS.' . preg_replace('/^SETTINGS./', '', $key), $default);
	}

	public static function get($key = null, $default = false)
	{
		if ($key) {
			if (Base::instance()->exists('SETTINGS.' . preg_replace('/^SETTINGS./', '', $key), $value)) {
				return $value;
			}
			return $default;
		}
		return Base::instance()->get('SETTINGS');
	}

	public static function defaults()
	{
		$app      = Base::instance();
		$defaults = [
			'_app'       => $app->get('PACKAGE'),
			'_ver'       => $app->get('VERSION'),
			'_time'      => $app->get('TIME'),
			'_domain'    => $app->get('SCHEME') . '://' . $app->get('HOST') . '/',
			'_url'       => $app->get('PATH'),
			'_uri'       => $app->get('URI'),
			'_query'     => $app->get('QUERY'),
			'_permalink' => $app->get('REALM'),
			'_segments'  => null,
			'_get'       => $app->get('GET') ?: null,
			'_post'      => $app->get('POST') ?: null,
			'_route'     => null,
			'_is_bot'    => Audit::instance()->isbot(),
			'_is_ajax'   => $app->get('AJAX'),
			'_is_mobile' => Audit::instance()->ismobile(),
			'_is_desktop'=> Audit::instance()->isdesktop(),
			'_flash'     => Flash::instance(),
			'_member'    => Addons\Member\Api::instance(),
			'_content'   => Addons\Content\Api::instance(),
			'_logged_in' => null,
		];
		$count = 0;
		foreach ($segments = explode('/', $app->get('PATH')) ?: [] as $seg) {
			if ($seg) {
				$count++;
				$defaults['_segments'][$count] = $seg;
				if ($seg === end($segments)) {
					$defaults['_segments']['_last'] = $seg;
				}
			}
		}
		return $defaults;
	}

	private function initKlay()
	{
		$app   = &$this->app;
		$cache = Cache::instance();
		// load initial app configuration
		$app->config(KLAY . 'config.ini', true);
		// check cache for and check integrity
		if ($cache->exists('config' . $app->get('KLAY.ext.cache'), $data) && $data['_integrity'] === $app->get('INTEGRITY')) {
			$config = $data;
		} else {
			if ($config = $app->read($app->cpath(KLAY, 'settings' . $app->get('KLAY.ext.config')))) {
				$config = Spyc::YAMLLoadString($config);
				if ($instance = $app->read($app->cpath(SITE, 'settings' . $app->get('KLAY.ext.config')))) {
					$config = array_replace_recursive($config, Spyc::YAMLLoadString($instance));
				}
				// env config
				if (isset($config['_environment'])) {
					$env = 'dev';
					foreach($config['_environment'] ?: [] as $group => $patterns) {
						foreach ($app->split($patterns) ?: [] as $pattern) {
							if (Helpers::stringMatch($pattern, $app->get('HOST'))) {
								$env = $group;
							}
						}
					}
					if ($environment = $app->read($app->cpath(SITE . 'settings.' . $env . $app->get('KLAY.ext.config')))) {
						$config['_environment'] = $env;
						$config = array_replace_recursive($config, Spyc::YAMLLoadString($environment));
					}
				}
				// theme config
				if (isset($config['_theme'])) {
					$theme = $app->cpath(SITE, $app->get('SITE.themes'), $config['_theme']['_name'], DIRECTORY_SEPARATOR);
					$config['_theme']['_root'] = $theme;
					if ($theme = $app->read($app->cpath($theme, 'theme' . $app->get('KLAY.ext.config')))) {
						$config['_theme'] = array_replace_recursive($config['_theme'], Spyc::YAMLLoadString($theme));
					}
				}
				// addon configs
				$config['_addons'] = [];
				$kaddons = $app->cpath(KLAY, 'core/addons/*/addon' . $app->get('KLAY.ext.config'));
				$saddons = $app->cpath(SITE, $app->get('SITE.addons'), '*/addon' . $app->get('KLAY.ext.config'));
				foreach (array_merge(Helpers::recursiveGlob($kaddons), Helpers::recursiveGlob($saddons)) ?: [] as $addon) { 
					$name = basename(dirname($addon));
					if (!array_key_exists($name, $config['_addons'] ?: [])) {
						$data = [
							'_root'      => trim(dirname($addon), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
							'_namespace' => str_replace([KLAY . 'core/', SITE, '/'], ['', '', '\\'], dirname($addon))
						];
						$config['_addons'][$name] = $data + Spyc::YAMLLoad($addon);
					}
				}
			}
			$config['_integrity'] = $app->get('INTEGRITY');
			$cache->set('config' . $app->get('KLAY.ext.cache'), $config);
		}
		// update application from configuration
		if (isset($config['_timezone'])) {
			$app->set('TZ', $config['_timezone']);
		}
		// update application language from config
		if (isset($config['_language'])) {
			$app->set('LANGUAGE', $config['_language']);
			foreach ($app->split($config['_language']) ?: [] as $lang) {
				if ($file = $app->read($app->cpath(SITE, $app->get('SITE.translations'), $lang . $app->get('KLAY.ext.config')))) {
					$config['_translations'] = Spyc::YAMLLoadString($file);
				}
			}
		}
		// set up addon triggers
		foreach($config['_addons'] ?: [] as $name => $addon) {
			// check for addon language files
			$langs = $app->cpath($addon['_root'], 'langs', DIRECTORY_SEPARATOR);
			if (is_dir($langs)) {
				$app->set('LOCALES', $app->get('LOCALES') . $langs . '|');
			}
			// check for addon trigger routes
			foreach($addon['_triggers'] ?: [] as $trigger) {
				$exec  = '/' . $app->cpath('TRIGGERS', $name, $trigger);
				$alias = $app->hash($exec);
				$app->route('GET|POST|HEAD @' . $alias . ': ' . $exec, $addon['_namespace'] . '\\triggers->' . $trigger);
			}
		}
		// set up custom routes
		foreach($config['_routes'] ?: [] as $i => $route) {
			$ttl   = (int) $route['_ttl'] ?: 0;
			$kbps  = (int) $route['_kbps'] ?: 0;
			$alias = $app->hash($route['_pattern']);
			$config['_routes'][$i]['_alias'] = $alias;
			$app->route('GET|POST|HEAD @' . $alias . ': ' . $route['_pattern'], $app->get('KLAY.router'), $ttl, $kbps);
		}
		// scrope config to hive
		$app->set('SETTINGS', $config);
	}
}

return Klay::instance();
