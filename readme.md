# Klay CMS
KlayCMS is a flat-file CMS, meaning we've done away with the database to rely on the file system to act as site data. This not only makes it 100% portable with low setup time, but it can be managed from your prefered IDE (hell even a basic text editor). Using YAML and Markdown/PUG to act as site data, it allows us to easily treat static files as if they were straight from a database.
---
---
[General Concept](#general-concept)  | [Getting Started](#getting-started) | [Routes](#routes) | [FrontMatter](#frontmatter)
---
---
## General Concept
KlayCMS was built from the ground up with an "Addon" economy in mind. It's light weight and allows you to extend it to whatever means you may need. It of course ships with an onslaught of core addons to provide the most basic functionality to build a website. Out of the box you'll be able to create routes, render views based on those routes, and access a rich API to send emails, register/login users, and read site content. You can even use Markdown and Pug as content and have these render correctly in your views.

## System Requirements
- **Web Server - Apache, Nginx, or IIS**
- **PHP >= 5.6** (7.x reccommended)
- **URL Rewriting Enabled** (mod_rewrite, try_files, etc...)
- **Composer** (recommened for development, optional for production if libraries are installed already)
---
&nbsp;
## **Getting Started**

### **1. Dependency Installation**
  - Install composer dependencies `cd klay/ && composer install`

### **2. Setup your `site/` directory**
  - `index.php` contains a relative path to your "front-end" folder typically this is `site/`. You may change this directory just specify the relative path to `index.php` for your custom directory. Remember to update your `.htaccess` file.

  - In your `site/` directory add a `settings.yaml` file. This file allows you to control your klay global configuration varialbes.

  - `site/` standard folder strucure:
    ```
    site/
    
        addons/
          <myaddon>/
            addon.yaml
            api.php
            filters.php
            macros.php
            triggers.php

        content/
          <group>/
            <group-page>.yaml
          <some-page>.yaml

        members/
          <member-name>.yaml

        themes/
          <my-theme>/
            css/
            img/
            js/
            layouts/
              default.html
            partials/
            templates/
              default.html
            theme.yaml

        translations/
          <lang-code>.yaml

        uploads/

        settings.yaml
    ```
&nbsp;
### **Routes**
The simplest way to get started is to establish some routes. Routes can be set in your `sites/settings.yaml`. Routes are the portion of the current URL after your domain `http://example.com/<this-is-your-route>`. Upon hitting one of your defined routes a certain layout and/or template will be rendered in which you can access the rich api of filters, macros, and triggers within addons to build a dynamic website.

`site/settings.yaml`:
```
...
# Site Routes
_routes        :
  -
    _pattern   : /
    _layout    : default
    _template  : default
    _access    : *
  ...
...
```

This will result in the creation of a homepage route, when a user goes to `http://<example>.com/` the system will render the `_template` value that is hopefully extending the `_layout` value.

!! *all layouts, partials, and templates should be `.html` files.* !!

&nbsp;
### **FrontMatter**
FrontMatter is the combination of YAML and Markdown/Pug. The YAML data is established between seperators `---` below the YAML you may use Markdown or Pug. 

The YAML contents are available to your Markdown/Pug as variables in their given syntax. Please review each syntaxes capabilities.

*!! **do not use pug extend and includes in frontmatter files** !!*

&nbsp;
example markdown:
```
---
title: Homepage
list:
  - of
  - items
---
# Welcome to the {{@title}}!
In cupidatat enim consectetur minim dolore enim. Eu dolor anim pariatur amet esse id cillum proident ullamco consequat sit incididunt. Culpa esse enim sint mollit non irure id et. Aute nulla in et voluptate anim cupidatat magna dolore. Ullamco excepteur id fugiat velit do reprehenderit esse. Nostrud eu labore sit ipsum sit ullamco est reprehenderit do. Et nisi fugiat ad consequat et non voluptate nisi Lorem et sint.
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Markdown Language](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

&nbsp;
example pug:
```
---
title: Homepage
list:
  - of
  - items
---
h1 Welcome to the #{title}!
p(class="intro") In cupidatat enim consectetur minim dolore enim. Eu dolor anim pariatur amet esse id cillum proident ullamco consequat sit incididunt. Culpa esse enim sint mollit non irure id et. Aute nulla in et voluptate anim cupidatat magna dolore. Ullamco excepteur id fugiat velit do reprehenderit esse. Nostrud eu labore sit ipsum sit ullamco est reprehenderit do. Et nisi fugiat ad consequat et non voluptate nisi Lorem et sint.
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Pug Language](https://pugjs.org/api/getting-started.html)

